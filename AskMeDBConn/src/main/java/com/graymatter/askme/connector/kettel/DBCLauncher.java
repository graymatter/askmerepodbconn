package com.graymatter.askme.connector.kettel;

import java.io.File;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.pentaho.di.core.Const;
import org.pentaho.di.core.xml.XMLHandler;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.graymatter.askme.common.exception.ASKPersistenceThrow;
import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.JobConstants;
import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.XMLEnvelopeConstants;
import com.graymatter.askme.connector.configuration.DBCEnvironment;
import com.graymatter.askme.connector.exception.DBCException;

public class DBCLauncher {
	private String path;
	
	public DBCLauncher(String path) {
		super();
		this.path = path;
	}

	public void launch() throws Exception{
		String xml = FileUtils.readFileToString(new File(path + File.separator + "conf.xml"));

		Document doc = XMLHandler.loadXMLString(xml);
		Node conf = XMLHandler.getSubNode(doc, XMLEnvelopeConstants.STARTER);
		List<Node> jobList = XMLHandler.getNodes(conf, XMLEnvelopeConstants.JOB_ENV);
		for(int i=0; i<jobList.size(); i++){
			try{
				Node job = jobList.get(i);
				DBCEnvironment.initConf(path, conf);
				DBCEnvironment.initJob(path,job);
				
				String disabled = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.JOB_ENV).get(JobConstants.ATTR_DISABLED);
				Boolean isDisabled = (Const.isEmpty(disabled) || "false".equalsIgnoreCase(disabled))?new Boolean(false):new Boolean(true);
				
				int dbConnectionAttemps = Integer.parseInt(DBCEnvironment.getEnv().get(XMLEnvelopeConstants.STARTER).get(XMLEnvelopeConstants.DB_CONN_ATTEMPS));

				if(!isDisabled){
					long startTime = System.currentTimeMillis();
					
					// DEVO CONTROLLARE L'ESITO DELL ULTIMA INDICIZZAZIONE PRENDENDO L'ULTIMO LOG
					
					DBCEnvironment.initLocalMongoCollection();
					DBCProcess process = new DBCProcess();
					process.execute();
					DBCEnvironment.writeRemoteMongoCollection(dbConnectionAttemps);
					
					long stopTime = System.currentTimeMillis();
			        long elapsedTime = stopTime - startTime;
			        
			        GregorianCalendar gc = new GregorianCalendar();
 			        gc.setTimeInMillis(elapsedTime);
 			        System.out.println("EXECUTION TIME --> " +  gc.get(Calendar.MINUTE) + ":" + gc.get(Calendar.SECOND) );
				}
			}catch(DBCException dbcExc){
				dbcExc.printStackTrace();
			}catch(ASKPersistenceThrow askPersistenceExc){
				askPersistenceExc.printStackTrace();
			}catch (Exception exc) {
				exc.printStackTrace();
			}finally{
				DBCEnvironment.release();
			}
		}

	}
	
	public static void main(String[] args) {
		try{
			DBCLauncher launcher = new DBCLauncher("F:\\Documenti\\Ask.Me\\DBConnector");
			launcher.launch();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
