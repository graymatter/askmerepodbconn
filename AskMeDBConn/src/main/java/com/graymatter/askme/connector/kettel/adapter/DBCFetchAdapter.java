package com.graymatter.askme.connector.kettel.adapter;

import java.sql.Connection;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.tika.Tika;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.bson.Document;
import org.pentaho.di.core.exception.KettleStepException;
import org.pentaho.di.core.logging.LogChannelInterface;
import org.pentaho.di.core.row.RowMeta;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.core.row.ValueMeta;
import org.pentaho.di.core.row.ValueMetaInterface;

import com.graymatter.askme.common.exception.ASKPersistenceThrow;
import com.graymatter.askme.common.utility.ASKConstants;
import com.graymatter.askme.common.utility.ASKConstants.ASKOutputConstants;
import com.graymatter.askme.common.utility.ASKConstants.ASKParameters;
import com.graymatter.askme.connector.bean.DBCDocument;
import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.JobConstants;
import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.XMLEnvelopeConstants;
import com.graymatter.askme.connector.configuration.DBCEnvironment;
import com.graymatter.askme.connector.kettel.general.DBCGeneralAdapter;
import com.graymatter.askme.connector.persistence.DBCFieldTableDAO;
import com.graymatter.askme.connector.persistence.DBCMongoConnectionManager;
import com.graymatter.askme.connector.util.DBCConstants;
import com.graymatter.askme.connector.util.DBCConstants.DBFetchConstants;
import com.graymatter.askme.connector.util.DBCConstants.SQLConstants;
import com.graymatter.askme.connector.util.DBCField;
import com.graymatter.askme.connector.util.fingerPrint.DBCFingerPrintGenerator;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;

public class DBCFetchAdapter extends DBCGeneralAdapter {
	private Connection connection = null;

	private boolean titleIsStream = true;
	private boolean contentIsStream = true;
	
	private String referenceFieldName;
	private String titleFieldName;
	private String contentFieldName;
	private int referenceIndex = -1;
	private int titleIndex = -1;
	private int contentIndex = -1;
	

	public DBCFetchAdapter(Connection connection,LogChannelInterface logger) {
		super(logger);
		this.connection = connection;
	}

	public void rowReadEvent(RowMetaInterface rowMeta, Object[] row) throws KettleStepException {
		//This step doesn't read data from previous node, therefore this method is not invoked.
	}
	
	private void manageFieldName(List<String> originalTableField) throws Exception{
		RowMetaInterface fieldTableMeta = new RowMeta();
		ValueMetaInterface fieldValueMeta = new ValueMeta();
		fieldValueMeta.setName("FIELD_NAME");
		fieldValueMeta.setOriginalColumnTypeName("VARCHAR");
		fieldTableMeta.addValueMeta(fieldValueMeta);
		DBCFieldTableDAO fieldTableDAO = new DBCFieldTableDAO(connection, fieldTableMeta, logger);
		fieldTableDAO.createTable();
		
		String[] currentRows = originalTableField.toArray(new String[originalTableField.size()]); 
		Object[] rows = fieldTableDAO.getRows(); 
		if(rows == null) {
			fieldTableDAO.insertFieldName(originalTableField);
		}else{
			String currentRowsToString = StringUtils.join(currentRows, "|");
			String rowToString = StringUtils.join(rows, "|");
			if(!currentRowsToString.equalsIgnoreCase(rowToString)){
				fieldTableDAO.replaceFieldName(originalTableField);
				DBCEnvironment.getEnv().get(XMLEnvelopeConstants.JOB_ENV).put(JobConstants.DROP_DBC_ROW_TABLE, "true");
			}
		}
	}

	public void rowWrittenEvent(RowMetaInterface rowMeta,Object[] row) throws KettleStepException {
		
		DBCMongoConnectionManager connectionManager = null;
		
		try {

			if (firstRow) {
				firstRow = false;
	
				List<Map<String, String>> fields = DBCEnvironment.getEnvFields();
				for(Map<String, String> currentField: fields){
					String attrName = currentField.get(DBCConstants.SQLConstants.ATTR_TAG);
					if(ASKOutputConstants.TAG_REFERENCE.equalsIgnoreCase(attrName)){
						referenceFieldName = DBCField.getAliasFromColumnName(currentField.get(DBCConstants.SQLConstants.FIELD));
					}
					if(ASKOutputConstants.TAG_TITLE.equalsIgnoreCase(attrName)){
						titleFieldName = DBCField.getAliasFromColumnName(currentField.get(DBCConstants.SQLConstants.FIELD));
					}
					if(ASKOutputConstants.TAG_CONTENT.equalsIgnoreCase(attrName)){
						contentFieldName = DBCField.getAliasFromColumnName(currentField.get(DBCConstants.SQLConstants.FIELD));
					}
				}
				
				List<String> originalTableField = new ArrayList<String>();
				for(Map<String, String> currentField: fields){
					String columnName = currentField.get(DBCConstants.SQLConstants.FIELD);
					columnName = DBCField.geColumnNameFromAlias(columnName);
					originalTableField.add(columnName);
				}
				originalTableField.add(DBCEnvironment.getEnv().get(XMLEnvelopeConstants.SQL_ENV).get(SQLConstants.PRIMARY_KEY));
				manageFieldName(originalTableField);
	
				for(int i=0; i<rowMeta.getFieldNames().length; i++){
					String name = rowMeta.getFieldNames()[i];
					if(referenceFieldName.equalsIgnoreCase(name)){
						referenceIndex = i;
					}
					if(titleFieldName.equalsIgnoreCase(name) || name.contains(titleFieldName.replace(",", ",'/',"))){
						titleIndex = i;
						int columnType = rowMeta.getValueMeta(i).getOriginalColumnType();
						if(columnType == Types.VARCHAR){
							titleIsStream = false;
						}
					}
					if(contentFieldName.equalsIgnoreCase(name)){
						contentIndex = i;
						int columnType = rowMeta.getValueMeta(i).getOriginalColumnType();
						if(columnType == Types.VARCHAR){
							contentIsStream = false;
						}
					}
				}
			}
			
			String reference = String.valueOf(row[referenceIndex]);
			
			String parsedTitle = "";
			byte[] title = (byte[])row[titleIndex];
			if(titleIsStream){
				Tika tika = new Tika();
				String type = tika.detect(title);
				logger.logDebug("Content TYPE: " + type);
				
				if("application/octet-stream".equalsIgnoreCase(type)){
					parsedTitle = new String(title, "UTF-8").replaceAll("[^\\p{ASCII}]", "").replaceAll("[\\p{Cc}\\p{Cf}\\p{Co}\\p{Cn}]", "");
				}else{
					tika.setMaxStringLength(title.length);
					Metadata metadata = new Metadata();
					TikaInputStream reader =  TikaInputStream.get(title, metadata);
					parsedTitle = tika.parseToString(reader, metadata);
				}
			}
			else{
				title = (byte[])row[titleIndex];
				if(title!=null)
					parsedTitle = new String(title, "UTF-8");
			}
			
			String parsedContent = "";
			byte[] content = (byte[])row[contentIndex];			
			if(contentIsStream){
				Tika tika = new Tika();
				String type = tika.detect(content);
				logger.logDebug("Content TYPE: " + type);
				
				if("application/octet-stream".equalsIgnoreCase(type)){
					parsedContent = new String(content, "UTF-8").replaceAll("[^\\p{ASCII}]", "").replaceAll("[\\p{Cc}\\p{Cf}\\p{Co}\\p{Cn}]", "");
				}else{
					tika.setMaxStringLength(content.length);
					Metadata metadata = new Metadata();
					TikaInputStream reader =  TikaInputStream.get(content, metadata);
					parsedContent = tika.parseToString(reader, metadata);
				}
			}
			else{
				content = (byte[])row[contentIndex];
				if(content!=null)
					parsedContent = new String(content, "UTF-8");
			}
			
			
			if(reference != null && reference.length() > 0 &&
			   parsedContent != null && parsedContent.length() > 0){
				String host = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.HOSTNAME);
				Integer port = Integer.parseInt(DBCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.PORT));
				String localDbName = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.DBNAME);
				
		        String fingerPrint = DBCFingerPrintGenerator.generateFingerPrint(parsedContent);
		        
				connectionManager = new DBCMongoConnectionManager(localDbName, host, port);
				String collectionName = DBFetchConstants.PREFIX +
									    DBCEnvironment.getEnv().get(XMLEnvelopeConstants.JOB_ENV).get(JobConstants.ATTR_NAME).replace(" ", "_") +
									    DBFetchConstants.SUFFIX;
				BasicDBObject query = new BasicDBObject();
				query.append(ASKOutputConstants.TAG_FINGERPRINT, fingerPrint);
				query.append(ASKConstants.ASKOutputConstants.TAG_REFERENCE, reference);
				
				FindIterable<Document> cursor = (FindIterable<Document>) connectionManager.find(collectionName, query);
				Iterator<Document> iter = cursor.iterator();
				
				// Trovo reference e fingerPrint...il documento non è cambiato
				if(iter.hasNext()) {
					Document currentDocument = iter.next();
					currentDocument.put(ASKConstants.ASKOutputConstants.TAG_REFERENCE, reference);
					currentDocument.put(ASKOutputConstants.TAG_TITLE, parsedTitle);
					currentDocument.put(ASKOutputConstants.TAG_FLAG, ASKParameters.FLAG_EQUAL);
					connectionManager.update(collectionName, query, currentDocument);
				}
				// Non trovo reference e fingerPrint...cerco solo con la reference
				else{
					query = new BasicDBObject(ASKConstants.ASKOutputConstants.TAG_REFERENCE, reference);
					cursor = (FindIterable<Document>) connectionManager.find(collectionName, query);
					iter = cursor.iterator();
					
					// Trovo la reference....è cambiato il content
					if(iter.hasNext()) {
						Document currentDocument = iter.next();
						currentDocument.put(ASKConstants.ASKOutputConstants.TAG_REFERENCE, reference);
						currentDocument.put(ASKOutputConstants.TAG_TITLE, parsedTitle);
						currentDocument.put(ASKOutputConstants.TAG_CONTENT, parsedContent);
						currentDocument.put(ASKOutputConstants.TAG_FINGERPRINT, fingerPrint);
						currentDocument.put(ASKOutputConstants.TAG_FLAG, ASKParameters.FLAG_UPDATE);
						connectionManager.update(collectionName, query, currentDocument);
					}
					// Non trovo la reference...il documento è nuovo
					else{
						DBCDocument currentFscDocument = new DBCDocument();
					    currentFscDocument.setReference(reference);
					    currentFscDocument.setTitle(parsedTitle);
					    currentFscDocument.setContent(parsedContent);
					    currentFscDocument.setFingerPrint(fingerPrint);
					    currentFscDocument.setFlag(ASKConstants.ASKParameters.FLAG_INSERT);
					    
					    Document bsonDocument = DBCDocument.createDBObject(currentFscDocument);
					    connectionManager.insert(collectionName, bsonDocument);
					}
				}
			}
		} catch (ASKPersistenceThrow e) {
			throw new KettleStepException(e);
		} catch (Exception e) {
			throw new KettleStepException(e);
		} finally{
			try {
				if(connectionManager != null) connectionManager.closeClient();
			} catch (ASKPersistenceThrow e) {
				e.printStackTrace();
			}
		}
	}

}
