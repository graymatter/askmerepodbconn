package com.graymatter.askme.connector.kettel.general;

import org.pentaho.di.core.logging.LogChannelInterface;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.trans.step.RowAdapter;

import com.graymatter.askme.common.utility.ASKConstants.ASKOutputConstants;

public abstract class DBCGeneralAdapter extends RowAdapter {
	protected boolean firstRow = true;
	protected LogChannelInterface logger;
	private Integer referenceIndex = null;

	public DBCGeneralAdapter(LogChannelInterface logger) {
		super();
		this.logger = logger;
	}

	protected Integer getReferenceIndex(RowMetaInterface rowMeta){
		if(referenceIndex==null){
			for(int i=0; i<rowMeta.getValueMetaList().size(); i++){
				String tagName = rowMeta.getValueMetaList().get(i).getName();
				if(ASKOutputConstants.TAG_REFERENCE.equals(tagName)){
					referenceIndex = i;
					break;
				}
			}
		}
		
		return referenceIndex;
	}

}
