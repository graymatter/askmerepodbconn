package com.graymatter.askme.connector.persistence;

import com.graymatter.askme.connector.exception.DBCAccessDataException;
import com.graymatter.askme.connector.persistence.general.DBCPersistenceOperator;

import java.sql.Connection;
import java.util.ArrayList;

import org.pentaho.di.core.logging.LogChannelInterface;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.core.row.ValueMetaInterface;

public class DBCEmbeddedTableDAO extends DBCPersistenceOperator {
	private static String TABLE = "DBC_ROW";
	private String GROUP_COLUMN =  "GROUP_";

	public DBCEmbeddedTableDAO(Connection connection, RowMetaInterface rowMeta, LogChannelInterface logger)
			throws DBCAccessDataException {
		super(TABLE, connection, rowMeta, logger);
		makeFields();
	}

	private void makeFields() throws DBCAccessDataException{
		String tableFields = "";
		for(int i=0; i<getRowMeta().getValueMetaList().size();i++){
			ValueMetaInterface valueMetaInterface = getRowMeta().getValueMeta(i);
			String colName = valueMetaInterface.getName();
			if(colName.startsWith("CONCAT(")) colName = GROUP_COLUMN + i;
			if(getColumnNames() == null) setColumnNames(new ArrayList<String>());
			getColumnNames().add(colName);
			
			tableFields = tableFields + colName + " " + valueMetaInterface.getOriginalColumnTypeName() + ",";
			if(getPK().equalsIgnoreCase(valueMetaInterface.getName())){
				tableFields = tableFields.replace(",", " PRIMARY KEY,");
			}
		}
		tableFields = (tableFields + "|").replace(",|", "");
		setTableFields(tableFields);
	}

}
