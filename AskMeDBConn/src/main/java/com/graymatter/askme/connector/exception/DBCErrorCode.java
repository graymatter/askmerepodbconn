package com.graymatter.askme.connector.exception;

public class DBCErrorCode {
	
	public class DBError{
		public static final String DB_GENERIC = 			"DBC-DB-000";
	}

	public class DBFetch{
		public static final String FETCH_GENERIC = 			"DBC-FETCH-000";
		public static final String FETCH_NOFIELDS = 		"DBC-FETCH-001"; //Non fields defined in configuration
	}
	
	public class DBMongoDB{
		public static final String MONGODB_GENERIC = 		"DBC-MONGODB-000";
	}

	public class Mapping{
		public static final String MAPPING_GENERIC = 		"DBC-MAPPING-000";
		
	}

	public class Output{
		public static final String OUTPUT_GENERIC = 		"DBC-OUTPUT-000";
		
	}

	public class Peristence{
		public static final String DBC_CONN_NULL 	= "DBC-000";//Connection NULL
		public static final String DBC_SELECT 		= "DBC-001";//Eccezione in fase di select
		public static final String DBC_INSERT 		= "DBC-002";//Eccezione in fase di insert
		public static final String DBC_UPDATE 		= "DBC-003";//Eccezione in fase di update
		public static final String DBC_DELETE 		= "DBC-004";//Eccezione in fase di delete
		public static final String DBC_CREATE 		= "DBC-005";//Exception during CREATE TABLE
		public static final String DBC_DESCR 		= "DBC-006";//Exception during DESCR TABLE
		public static final String DBC_TRUNCATE 	= "DBC-007";//Exception during TRUNCATE TABLE
		public static final String DBC_DROP 		= "DBC-008";//Exception during DROP TABLE
	}
	
	public class Encoding{
		public static final String ENCODING_GENERIC = 		"FSC-ENCODING-000";
	}

}
