package com.graymatter.askme.connector.util.fingerPrint;

import java.io.FileInputStream;
import java.io.Serializable;

import org.apache.commons.codec.digest.DigestUtils;

import com.graymatter.askme.connector.exception.DBCErrorCode;
import com.graymatter.askme.connector.exception.DBCException;

public class DBCFingerPrintGenerator implements Serializable{

	private static final long serialVersionUID = 1L;
	

	public DBCFingerPrintGenerator() {
		super();
	}
	
	
	public static String generateFingerPrint(FileInputStream file) throws Exception {
		
		String fingerPrint = null;
		try{
			fingerPrint = DigestUtils.md5Hex(file);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBCException(DBCErrorCode.Encoding.ENCODING_GENERIC);
		} 
		
		return fingerPrint;
	}
	
	public static String generateFingerPrint(String file) throws Exception {
		
		String fingerPrint = null;
		try{
			fingerPrint = DigestUtils.md5Hex(file);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBCException(DBCErrorCode.Encoding.ENCODING_GENERIC);
		} 
		
		return fingerPrint;
	}
}
