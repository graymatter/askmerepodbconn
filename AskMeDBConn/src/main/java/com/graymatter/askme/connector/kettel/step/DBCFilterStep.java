package com.graymatter.askme.connector.kettel.step;

import org.pentaho.di.core.Condition;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.di.trans.steps.filterrows.FilterRowsMeta;

import com.graymatter.askme.common.utility.ASKConstants.ASKOutputConstants;
import com.graymatter.askme.connector.exception.DBCErrorCode;
import com.graymatter.askme.connector.exception.DBCException;
import com.graymatter.askme.connector.kettel.general.DBCGeneralStep;

public class DBCFilterStep extends DBCGeneralStep {
	
	private Condition getCondition(){
		
		Condition condition = new Condition();
		condition.setLeftValuename(ASKOutputConstants.TAG_REFERENCE);
		condition.setFunction(Condition.FUNC_NOT_NULL);
		return condition;
	}

	@Override
	public StepMetaInterface getStep() throws Exception {
		FilterRowsMeta filterRowsMeta = null;
		try {
			filterRowsMeta = new FilterRowsMeta();
			filterRowsMeta.setCondition(getCondition());
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBCException(DBCErrorCode.Mapping.MAPPING_GENERIC);
		} 
		return filterRowsMeta;
	}

}
