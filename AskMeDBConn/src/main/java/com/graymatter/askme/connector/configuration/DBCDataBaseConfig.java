package com.graymatter.askme.connector.configuration;

import org.pentaho.di.core.Const;

import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.XMLEnvelopeConstants;
import com.graymatter.askme.connector.util.DBCConstants;
import com.graymatter.askme.connector.util.DBCConstants.DBFetchConstants;

public class DBCDataBaseConfig {
	private String TEMPLATE = "<connection>" + Const.CR 
			+ "<name>DATASOURCE</name>" + Const.CR
			+ "<server>[" + XMLEnvelopeConstants.HOSTNAME + "]</server>" + Const.CR
			+ "<type>[" + XMLEnvelopeConstants.DBTYPE +"]</type>" +  Const.CR
			+ "<access>[" + XMLEnvelopeConstants.ACCESS  + "]</access>" + Const.CR 
			+ "<database>[" + XMLEnvelopeConstants.DBNAME + "]</database>" + Const.CR
			+ "<port>[" + XMLEnvelopeConstants.PORT + "]</port>" + Const.CR 
			+ "<username>[" + XMLEnvelopeConstants.USER + "]</username>" + Const.CR
			+ "<password>[" + XMLEnvelopeConstants.PWD +"]</password>"+ Const.CR 
			+ "</connection>";
	
	public String getStringConnection(){
		String templateComplete = TEMPLATE;
		for(String key: DBCEnvironment.getEnv().get(DBFetchConstants.DB_TO_FETCH).keySet()){
			String value = DBCEnvironment.getEnv().get(DBFetchConstants.DB_TO_FETCH).get(key);
			templateComplete = templateComplete.replace("[" + key + "]", value);
		}
		return templateComplete;
	}

}
