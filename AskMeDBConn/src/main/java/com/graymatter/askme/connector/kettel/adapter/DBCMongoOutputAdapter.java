package com.graymatter.askme.connector.kettel.adapter;

import org.bson.Document;
import org.pentaho.di.core.exception.KettleStepException;
import org.pentaho.di.core.logging.LogChannelInterface;
import org.pentaho.di.core.row.RowMetaInterface;

import com.graymatter.askme.common.exception.ASKPersistenceThrow;
import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.JobConstants;
import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.XMLEnvelopeConstants;
import com.graymatter.askme.connector.configuration.DBCEnvironment;
import com.graymatter.askme.connector.kettel.general.DBCGeneralAdapter;
import com.graymatter.askme.connector.persistence.DBCMongoConnectionManager;
import com.graymatter.askme.connector.util.DBCConstants.DBFetchConstants;
import com.mongodb.client.MongoCollection;

public class DBCMongoOutputAdapter extends DBCGeneralAdapter {
	
	public DBCMongoOutputAdapter(LogChannelInterface logger) {
		super(logger);
	}

	
	public void rowReadEvent(RowMetaInterface rowMeta, Object[] row) throws KettleStepException {
		System.out.print("OUTPUT ADAPTER READ");
	}
	
	public void rowWrittenEvent(RowMetaInterface rowMeta,Object[] row) throws KettleStepException {
		
		DBCMongoConnectionManager localConnectionManager = null;
		DBCMongoConnectionManager remoteConnectionManager = null;
		
		try{
			String localHost = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.HOSTNAME);
			Integer localPort = Integer.parseInt(DBCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.PORT));
			String localDbName = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.DBNAME);
			
			String remoteHost = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.REMOTE_DATABASE_ENV).get(XMLEnvelopeConstants.HOSTNAME);
			Integer remotePort = Integer.parseInt(DBCEnvironment.getEnv().get(XMLEnvelopeConstants.REMOTE_DATABASE_ENV).get(XMLEnvelopeConstants.PORT));
			String remoteDbName = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.REMOTE_DATABASE_ENV).get(XMLEnvelopeConstants.DBNAME);
			
			String collectionName = DBFetchConstants.PREFIX +
								    DBCEnvironment.getEnv().get(XMLEnvelopeConstants.JOB_ENV).get(JobConstants.ATTR_NAME).replace(" ", "_") +
								    DBFetchConstants.SUFFIX;
			
			localConnectionManager = new DBCMongoConnectionManager(localDbName, localHost, localPort);
			MongoCollection<Document> localContentTableCollection = localConnectionManager.getCollection(collectionName);
			
			if(localContentTableCollection.count() > 0){
				remoteConnectionManager = new DBCMongoConnectionManager(remoteDbName, remoteHost, remotePort);
				remoteConnectionManager.createCollection(localContentTableCollection, collectionName);
			}
		} 
		catch (ASKPersistenceThrow e) {
			throw new KettleStepException(e);
		}
		catch (Exception exc){
			exc.printStackTrace();
			throw new KettleStepException(exc);
		}
		finally{
			try {
				if(localConnectionManager != null) localConnectionManager.closeClient();
				if(remoteConnectionManager != null) remoteConnectionManager.closeClient();
			} catch (ASKPersistenceThrow e) {
				e.printStackTrace();
			}
		}
	}
}
