package com.graymatter.askme.connector.util;


public class DBCConstants {
	
	public static final String DBC_LOG_TABLE =  "DBC_LOG_TABLE";

	public static class SQLConstants{
		public static final String SELECT = 			"select";
		public static final String PRIMARY_KEY = 		"primaryKey";
		public static final String FIELDS = 			"fields";
		public static final String FIELD =	 			"field";
		public static final String FIELD_SEPARATOR =	",";

		public static final String OPERATOR_AS =		" AS ";
		public static final String OPERATOR_WHERE =		"WHERE";

		public static String ATTR_TAG = 			"tag";
		public static String ATTR_TYPE = 			"type";
		public static String ATTR_SEPARATOR = 		"separator";
		public static String ATTR_FORMAT = 			"format";
		
		
		public static String[] getAttribute(){
			return new String[]{ATTR_TYPE, ATTR_FORMAT, ATTR_SEPARATOR, ATTR_TAG};
		}
	}
	
	public static class DBFetchConstants{
		public static final String DB_TO_FETCH = 	"databaseToFetch";
		
		public static final String CONN_STRING = 	"jdbc:h2:file:[PATH]/data/[NAME]";
		public static final String CONN_USER = 		"GRAY";
		public static final String CONN_PWD = 		"MATTER";
		
		public static final String PREFIX =			"DBC_";
		public static final String SUFFIX =			"_TABLE";
		
	}

}
