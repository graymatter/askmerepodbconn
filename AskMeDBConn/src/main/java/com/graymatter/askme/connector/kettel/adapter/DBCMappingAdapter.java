package com.graymatter.askme.connector.kettel.adapter;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.pentaho.di.core.Const;
import org.pentaho.di.core.exception.KettleStepException;
import org.pentaho.di.core.logging.LogChannelInterface;
import org.pentaho.di.core.row.RowMetaInterface;

import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.JobConstants;
import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.XMLEnvelopeConstants;
import com.graymatter.askme.connector.configuration.DBCEnvironment;
import com.graymatter.askme.connector.kettel.general.DBCGeneralAdapter;
import com.graymatter.askme.connector.persistence.DBCEmbeddedTableDAO;

public class DBCMappingAdapter extends DBCGeneralAdapter {
	private Connection connection = null;
	private boolean skipRow = false;
	private boolean isIncremental = false;
	
	public DBCMappingAdapter(Connection connection, LogChannelInterface logger) {
		super(logger);
		this.connection = connection;
		isIncremental = isIncremental().booleanValue();
	}
	
	private Boolean isIncremental(){
		String incremental = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.JOB_ENV).get(JobConstants.ATTR_INCREMENTAL);
		Boolean isIncremental = (Const.isEmpty(incremental) || !"true".equalsIgnoreCase(incremental))?new Boolean(false):new Boolean(true);
		return isIncremental;
	}
	
	private Object[] getClearedRow(Object[] row) throws Exception{
		Object[] clearedRow = null;
		List<Object> l = new ArrayList<Object>();
		for(int i=1; i<row.length; i++){
			if(row[i]!=null){
				try{
					l.add(new String((byte[])row[i]));
				}catch(Exception e){
					l.add(row[i]);
				}
			}
		}
		clearedRow = l.toArray(new Object[l.size()]);
		
		return clearedRow;
	}

	public void rowReadEvent(RowMetaInterface rowMeta, Object[] row) throws KettleStepException {
		try{
			DBCEmbeddedTableDAO embeddedTableDAO = new DBCEmbeddedTableDAO(connection, rowMeta, logger);

			if(firstRow){
				String forceDrop = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.JOB_ENV).get(JobConstants.DROP_DBC_ROW_TABLE);
				Boolean flagForceDrop = (forceDrop == null)?new Boolean(false):new Boolean(forceDrop);
				if(flagForceDrop) embeddedTableDAO.drop();
				
				embeddedTableDAO.createTable();
				firstRow = false;
			}
			
			skipRow = false;
			Object[] insideRowByPk = embeddedTableDAO.getRowByPk(row[0]);
			if(insideRowByPk==null){
				embeddedTableDAO.insert(row);
			}else{
				//Verify if row and insideRowByPk is the same object, if it's the same, row have to be ignored!
				String stringFromInsideDB = StringUtils.join(getClearedRow(insideRowByPk), ",");
				String stringFromSource = StringUtils.join(getClearedRow(row), ",");
				if(stringFromInsideDB.equals(stringFromSource)){
					if(isIncremental) skipRow = true; //row is ignored only if job is an incremental job.
				}else{
					//The row from source is newer row from inside db. Row in inside db has to update.
					embeddedTableDAO.updateByPk(row, row[0]);
				}
			}
		}catch(Exception e){
			throw new KettleStepException(e.toString());
		}
	}

	public void rowWrittenEvent(RowMetaInterface rowMeta, Object[] row) throws KettleStepException {
		if(skipRow) row[getReferenceIndex(rowMeta)]=null;
	}

}
