package com.graymatter.askme.connector.persistence;

import java.sql.Connection;
import java.sql.DriverManager;

import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.JobConstants;
import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.XMLEnvelopeConstants;
import com.graymatter.askme.connector.configuration.DBCEnvironment;
import com.graymatter.askme.connector.util.DBCConstants.DBFetchConstants;

public class DBCEmbedConnectionManager {
	private Connection connection;

	public DBCEmbedConnectionManager() throws Exception{
		super();
        Class.forName("org.h2.Driver");
	}
	
	public Connection getConnection() throws Exception{
		if(connection==null){
			String path = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.JOB_ENV).get(JobConstants.PATH);
			String jobName = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.JOB_ENV).get(JobConstants.ATTR_NAME).replace(" ", "_" );

			// I VALORI VANNO PRESI IN BASE AI PARAMETRI DEL FILE DI CONFIGURAZIONE
			// COSTRUIRE STRINGA DI CONNESSIONE DINAMICA
			String connectionUrl = DBFetchConstants.CONN_STRING.replace("[PATH]", path).replace("[NAME]", jobName);
			String user = DBFetchConstants.CONN_USER;
			String pwd = DBFetchConstants.CONN_PWD;
			connection = DriverManager.getConnection(connectionUrl, user, pwd);
		}
		return connection;
	}

	public void closeConnection(){
		try{
			if(connection!=null) connection.close();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			connection = null;
		}
	}
	
}
