package com.graymatter.askme.connector.kettel.step;

import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.di.trans.steps.splitfieldtorows.SplitFieldToRowsMeta;

import com.graymatter.askme.common.utility.ASKConstants.ASKOutputConstants;
import com.graymatter.askme.connector.exception.DBCErrorCode;
import com.graymatter.askme.connector.exception.DBCException;
import com.graymatter.askme.connector.kettel.general.DBCGeneralStep;

public class DBCSplitContentStep extends DBCGeneralStep{
	private String sectionDelimiter;
	
	public DBCSplitContentStep() {
		super();
	}
	
	public DBCSplitContentStep(String sectionDelimiter) {
		super();
		this.sectionDelimiter = sectionDelimiter;
	}
	

	public StepMetaInterface getStep() throws Exception {
		SplitFieldToRowsMeta sfrw = null;
		try{
			sfrw = new SplitFieldToRowsMeta();
			sfrw.setDefault();
			sfrw.setSplitField(ASKOutputConstants.TAG_CONTENT);
			sfrw.setNewFieldname(ASKOutputConstants.TAG_SECTION_CONTENT);
			sfrw.setDelimiter(sectionDelimiter);
			sfrw.setIncludeRowNumber(true);
			sfrw.setRowNumberField(ASKOutputConstants.TAG_SECTION);
			sfrw.setResetRowNumber(true);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBCException(DBCErrorCode.Output.OUTPUT_GENERIC);
		} 
		return sfrw;
	}
}
