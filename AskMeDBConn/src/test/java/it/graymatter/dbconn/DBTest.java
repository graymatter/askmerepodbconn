package it.graymatter.dbconn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DBTest {

	public static void main(String[] args) {
		 try
	        {
	            Class.forName("org.h2.Driver");
	            Connection con = DriverManager.getConnection("jdbc:h2:file:/Users/massimilianopolito/Documents/PROGETTI/Connettori/DBConn/data/Estrazione_da_Tabella_di_Test", "GRAY", "MATTER" );
	            Statement stmt = con.createStatement();
	            //stmt.executeUpdate( "DROP TABLE table1" );
	           // stmt.executeUpdate( "CREATE TABLE IF NOT EXISTS EMBEDDED_TABLE(ID INT,TITLE VARCHAR,CONTENT LONGBLOB);");
	           // stmt.executeUpdate( "INSERT INTO table1 ( user ) VALUES ( 'test' )" );
	           // stmt.executeUpdate( "INSERT INTO table1 ( user ) VALUES ( 'di immissione' )" );
	 
	            ResultSet rs = stmt.executeQuery("SELECT * FROM EMBEDDED_TABLE");
	            while( rs.next() )
	            {
	                Object name = rs.getBytes("TITLE");
	                System.out.println(new String((byte[])name));
	            }
	            stmt.close();
	            con.close();
	        }
	        catch( Exception e )
	        {
	            System.out.println( e.getMessage() );
	        }
	}

}
