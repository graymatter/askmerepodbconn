package com.graymatter.askme.connector.kettel.step;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.pentaho.di.core.Const;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.di.trans.steps.xmloutput.XMLField;
import org.pentaho.di.trans.steps.xmloutput.XMLOutputMeta;

import com.graymatter.askme.common.utility.ASKConstants.ASKOutputConstants;
import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.JobConstants;
import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.XMLEnvelopeConstants;
import com.graymatter.askme.connector.configuration.DBCEnvironment;
import com.graymatter.askme.connector.exception.DBCErrorCode;
import com.graymatter.askme.connector.exception.DBCException;
import com.graymatter.askme.connector.kettel.general.DBCGeneralStep;
import com.graymatter.askme.connector.util.DBCConstants.SQLConstants;

public class DBCOutputStep extends DBCGeneralStep{
	private String ext = "xml";
	private String[] targetFields;
	private int contentIndex;
	
	public DBCOutputStep(String[] targetFields) {
		super();
		this.targetFields = targetFields;
	}
	
	private XMLField [] makeField() throws Exception{
		List<Map<String, String>> fields = DBCEnvironment.getEnvFields();
		DBCEnvironment.getEnv();
		
		XMLField [] xf = new XMLField[targetFields.length];
		for(int i=0; i<fields.size(); i++){
			
//			if(!targetFields[i].equalsIgnoreCase(ASKOutputConstants.TAG_CONTENT)){
				xf[i] = new XMLField(); 
				xf[i].setFieldName(targetFields[i]);
				
				String type = fields.get(i).get(SQLConstants.ATTR_TYPE);
				if(!Const.isEmpty(type)) xf[i].setType(type);
	
				String format = fields.get(i).get(SQLConstants.ATTR_FORMAT);
				if(!Const.isEmpty(format)) xf[i].setFormat(format);
//			}
//			else{
//				contentIndex = i;
//			}
		}
		
//		xf[contentIndex] = new XMLField(); 
//		xf[contentIndex].setFieldName(ASKOutputConstants.TAG_SECTION_CONTENT);
//				
//		xf[targetFields.length] = new XMLField(); 
//		xf[targetFields.length].setFieldName(ASKOutputConstants.TAG_SECTION);
		
		return xf;
	}

	public StepMetaInterface getStep() throws Exception {
		XMLOutputMeta xom = null;
		
		try{
			String path = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.JOB_ENV).get(JobConstants.PATH);
			String jobName = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.JOB_ENV).get(JobConstants.ATTR_NAME);
			String fileName = path + File.separator + ASKOutputConstants.FILENAME.replace("@", jobName);

			xom = new XMLOutputMeta();
			xom.setMainElement(ASKOutputConstants.ROOT);
			xom.setRepeatElement(ASKOutputConstants.CONTAINER);
			xom.setExtension(ext);
			xom.setDateInFilename(true);
			xom.setTimeInFilename(true);
			xom.setFileName(fileName);
			xom.setOutputFields(makeField());
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBCException(DBCErrorCode.Output.OUTPUT_GENERIC);
		} 
		return xom;
	}

}
