package com.graymatter.askme.connector.exception;

@SuppressWarnings("serial")
public class DBCException extends Exception {

	public DBCException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DBCException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DBCException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DBCException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
