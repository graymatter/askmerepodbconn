package com.graymatter.askme.connector.persistence.general;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.h2.jdbc.JdbcBlob;
import org.h2.jdbc.JdbcClob;
import org.pentaho.di.core.logging.LogChannelInterface;
import org.pentaho.di.core.row.RowMetaInterface;

import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.XMLEnvelopeConstants;
import com.graymatter.askme.connector.configuration.DBCEnvironment;
import com.graymatter.askme.connector.exception.DBCAccessDataException;
import com.graymatter.askme.connector.exception.DBCErrorCode.Peristence;
import com.graymatter.askme.connector.util.DBCConstants.SQLConstants;

public abstract class DBCPersistenceOperator {
	private String CREATE = 		"CREATE TABLE IF NOT EXISTS [TABLE](@);";
	private String INSERT = 		"INSERT INTO [TABLE] VALUES(@);";
	private String SELECT = 		"SELECT * FROM [TABLE]";
	private String SELECT_BY_PK = 	"SELECT * FROM [TABLE] WHERE [PK] = ?;";
	private String UPDATE_BY_PK = 	"UPDATE [TABLE] SET [FIELDS] WHERE [PK] = ?;";
	private String DESCRIBE = 		"SHOW COLUMNS FROM [TABLE];"; 
	private String DROP = 			"DROP TABLE [TABLE]";
	private String TRUNCATE = 		"TRUNCATE TABLE [TABLE]";

	private Connection connection;
	private String primaryKey = "";
	
	private String tableFields = "";
	private RowMetaInterface rowMeta;
	private List<String> columnNames;
	
	private LogChannelInterface logger;
	
	public DBCPersistenceOperator(String tableName,
								  Connection connection, 
								  RowMetaInterface rowMeta, 
								  LogChannelInterface logger) throws DBCAccessDataException{
		super();
		this.connection = connection;
		this.rowMeta = rowMeta;
		this.logger = logger;
		
		CREATE = CREATE.replace("[TABLE]", tableName);
		INSERT = INSERT.replace("[TABLE]", tableName);
		SELECT_BY_PK = SELECT_BY_PK.replace("[TABLE]", tableName);
		UPDATE_BY_PK = UPDATE_BY_PK.replace("[TABLE]", tableName);
		DESCRIBE = DESCRIBE.replace("[TABLE]", tableName);
		SELECT = SELECT.replace("[TABLE]", tableName);
		DROP = DROP.replace("[TABLE]", tableName);
		TRUNCATE = TRUNCATE.replace("[TABLE]", tableName);
		
	}
	
	private void makeLog(String message){
		if(logger!=null) logger.logDebug(message);
	}
	
	protected String getPK(){
		primaryKey = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.SQL_ENV).get(SQLConstants.PRIMARY_KEY);
		return primaryKey;
	}
	
	protected String getTableFields() {return tableFields;}
	protected void setTableFields(String tableFields) {this.tableFields = tableFields;}

	protected RowMetaInterface getRowMeta() {return rowMeta;}
	protected void setRowMeta(RowMetaInterface rowMeta) {this.rowMeta = rowMeta;}

	protected List<String> getColumnNames() {return columnNames;}
	protected void setColumnNames(List<String> columnNames) {this.columnNames = columnNames;}

	private Connection getConnection() throws DBCAccessDataException{
		if(connection == null){
			throw new DBCAccessDataException(Peristence.DBC_CONN_NULL);
		}
		return connection;
	}
	
	private void atomicOperation(String sql) throws Exception{
		Statement stmt = null;
		try{
            stmt = getConnection().createStatement();
            stmt.execute(sql);
            makeLog(sql);
		}finally{
			if(stmt!=null) stmt.close();
		}
	}
	
	public void describe() throws DBCAccessDataException{		
		try{
			Statement stmt = null;
			ResultSet rs = null;
			try{
	            stmt = getConnection().createStatement();
	            rs = stmt.executeQuery(DESCRIBE);
	            while(rs.next()){
	            	System.err.println(rs.getString("COLUMN_NAME"));
	            }
			}finally{
				if(rs!=null) rs.close();
				if(stmt!=null) stmt.close();
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new DBCAccessDataException(Peristence.DBC_DESCR, e);
		}
	}
	
	public void truncate() throws DBCAccessDataException{	
		try{
			atomicOperation(TRUNCATE);
		}catch(Exception e){
			e.printStackTrace();
			throw new DBCAccessDataException(Peristence.DBC_TRUNCATE, e);
		}
	}

	public void drop() throws DBCAccessDataException{	
		try{
			atomicOperation(DROP);
		}catch(Exception e){
			e.printStackTrace();
			throw new DBCAccessDataException(Peristence.DBC_TRUNCATE, e);
		}
	}

	public void createTable() throws DBCAccessDataException{
		Statement stmt = null;
		try{
			try{
				CREATE = CREATE.replace("@", tableFields);
	            stmt = getConnection().createStatement();
	            stmt.executeUpdate(CREATE);
	            makeLog(CREATE);
			}finally{
				if(stmt!=null) stmt.close();
			}
 		}catch(Exception e){
			e.printStackTrace();
			throw new DBCAccessDataException(Peristence.DBC_CREATE, e);
 		}
	}
	
	public void insert(Object[] row) throws  DBCAccessDataException{
		try{
			PreparedStatement ps = null;
			try{
				String values = "";
				for(int i = 0; i<columnNames.size(); i++){
					values =  values + "?,";
				}
				values = (values + "|").replace(",|", "");
				INSERT = INSERT.replace("@", values);
				
				ps = getConnection().prepareStatement(INSERT);
				for(int i=0; i<columnNames.size(); i++){
					Object value = row[i];
					ps.setObject(i+1, value);
				}
				ps.execute();
		
			}finally{
				if(ps!=null) ps.close();
			}
 		}catch(Exception e){
			e.printStackTrace();
			throw new DBCAccessDataException(Peristence.DBC_INSERT, e);
 		}
	}

	public Object[] getRows() throws DBCAccessDataException{
		try{
			Object[] row = null;
			ResultSet rs = null;
			PreparedStatement ps = null;
			try{
				ps = getConnection().prepareStatement(SELECT);
				
				rs = ps.executeQuery();
				ResultSetMetaData rsmd = rs.getMetaData();
				int columnCount = rsmd.getColumnCount();
	
				List<Object> listResult = null;
				while(rs.next()){
					if(listResult==null) listResult = new ArrayList<Object>();
					for (int i = 1; i < columnCount + 1; i++ ) {
						Object obj = rs.getObject(i);
						listResult.add(obj);
					}
				}
				
				if(listResult!=null) row = listResult.toArray(new Object[listResult.size()]);
				
				return row;
			}finally{
				if(rs!=null) rs.close();
				if(ps!=null) ps.close();
			}
 		}catch(Exception e){
			e.printStackTrace();
			throw new DBCAccessDataException(Peristence.DBC_SELECT, e);
 		}
	}

	public Object[] getRowByPk(Object pkValue) throws DBCAccessDataException{
		try{
			Object[] row = null;
			ResultSet rs = null;
			PreparedStatement ps = null;
			try{
				SELECT_BY_PK = SELECT_BY_PK.replace("[PK]", getPK());
				
				ps = getConnection().prepareStatement(SELECT_BY_PK);
				ps.setObject(1, pkValue);
				
				rs = ps.executeQuery();
				ResultSetMetaData rsmd = rs.getMetaData();
				int columnCount = rsmd.getColumnCount();
	
				List<Object> listResult = null;
				if(rs.next()){
					if(listResult==null) listResult = new ArrayList<Object>();
					for (int i = 1; i < columnCount + 1; i++ ) {
						Object obj = rs.getObject(i);
						if(obj instanceof JdbcBlob || obj instanceof JdbcClob || obj instanceof String) obj = rs.getBytes(i);
						listResult.add(obj);
					}
				}
				
				if(listResult!=null) row = listResult.toArray(new Object[listResult.size()]);
				
				return row;
			}finally{
				if(rs!=null) rs.close();
				if(ps!=null) ps.close();
			}
 		}catch(Exception e){
			e.printStackTrace();
			throw new DBCAccessDataException(Peristence.DBC_SELECT, e);
 		}
	}
	
	public void updateByPk(Object[] row, Object pkValue) throws DBCAccessDataException{
		try{
			PreparedStatement ps = null;
			try{
				UPDATE_BY_PK = UPDATE_BY_PK.replace("[PK]", getPK());
				
				String fields = "";
				for(int i=0; i<columnNames.size();i++){
					fields = fields + columnNames.get(i) + "=?,";
				}
				fields = (fields+"|").replace(",|", "");
				UPDATE_BY_PK = UPDATE_BY_PK.replace("[FIELDS]", fields);
				
				ps = getConnection().prepareStatement(UPDATE_BY_PK);
				
				int i = -1;
				for(i=0; i<columnNames.size(); i++){
					Object value = row[i];
					ps.setObject(i+1, value);
				}
				ps.setObject(i+1, pkValue);
				ps.execute();
				
			}finally{
				if(ps!=null) ps.close();
			}
 		}catch(Exception e){
			e.printStackTrace();
			throw new DBCAccessDataException(Peristence.DBC_UPDATE, e);
 		}
	}

}
