package com.graymatter.askme.connector.kettel;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.pentaho.di.core.Const;
import org.pentaho.di.core.KettleEnvironment;
import org.pentaho.di.core.Result;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.logging.LogLevel;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransHopMeta;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.StepInterface;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;

import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.JobConstants;
import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.XMLEnvelopeConstants;
import com.graymatter.askme.connector.configuration.DBCEnvironment;
import com.graymatter.askme.connector.database.DBCDataBaseStep;
import com.graymatter.askme.connector.exception.DBCException;
import com.graymatter.askme.connector.kettel.adapter.DBCFetchAdapter;
import com.graymatter.askme.connector.kettel.adapter.DBCFilterAdapter;
import com.graymatter.askme.connector.kettel.adapter.DBCMappingAdapter;
import com.graymatter.askme.connector.kettel.adapter.DBCMongoOutputAdapter;
import com.graymatter.askme.connector.kettel.general.DBCGeneralStep;
import com.graymatter.askme.connector.kettel.step.DBCFetchStep;
import com.graymatter.askme.connector.kettel.step.DBCFilterStep;
import com.graymatter.askme.connector.kettel.step.DBCMappingStep;
import com.graymatter.askme.connector.kettel.step.DBCMongoInputStep;
import com.graymatter.askme.connector.kettel.step.DBCMongoOutputStep;
import com.graymatter.askme.connector.persistence.DBCEmbedConnectionManager;

public class DBCProcess {
	DatabaseMeta dataBaseMeta;
	Map<String, StepMeta> mapSteps = new LinkedHashMap<String, StepMeta>();

	private StepMeta getStepMeta(StepMetaInterface step, String name, String description) throws Exception{
		StepMeta stepMeta = new StepMeta(name, step);
		stepMeta.setDescription(description);
		return stepMeta;
	}
	
	private StepMeta addStep (TransMeta currentTrans, DBCGeneralStep currentStep, String stepName, String stepDescription) throws Exception{
		StepMetaInterface step = currentStep.getStep();
		StepMeta stepMeta = getStepMeta(step, stepName, stepDescription);
		currentTrans.addStep(stepMeta);
		return stepMeta;
	}

	private TransHopMeta getHop(StepMeta from, StepMeta to) throws Exception{
		TransHopMeta hop = new TransHopMeta(from, to);
		return hop;
	}
	
	private void createHop(TransMeta currentTrans) throws Exception{
		Set<String> keySet = mapSteps.keySet();
		ArrayList<String> keyList = new ArrayList<String>(keySet);
		for(int i=0; i<keyList.size()-1; i++){
			TransHopMeta hop = getHop(mapSteps.get(keyList.get(i)), mapSteps.get(keyList.get(i+1)));
			currentTrans.addTransHop(hop);
		}
	}
	
	private TransMeta getTransDefinition() throws Exception{
		TransMeta transMeta = new TransMeta();
		try {
			transMeta.setName(DBCEnvironment.getEnv().get(XMLEnvelopeConstants.JOB_ENV).get(JobConstants.ATTR_NAME));
			
			DBCDataBaseStep dataBaseStep = new DBCDataBaseStep();
			dataBaseMeta = dataBaseStep.getDataBaseObject();
			
//			mapSteps.put("MONGODB INPUT", addStep(transMeta, new DBCMongoInputStep(), "MONGODB INPUT", "Reading from ServerDB"));
			mapSteps.put("FETCH DB", addStep(transMeta,new DBCFetchStep(dataBaseMeta),"FETCH DB","Reading from table in DB"));
//			mapSteps.put("MAPPING", addStep(transMeta,new DBCMappingStep(),"MAPPING","Create association from Table Column and XML tag"));
			mapSteps.put("FILTER", addStep(transMeta,new DBCFilterStep(),"FILTER","Remove row if reference is null"));
//			mapSteps.put("MONGODB OUTPUT", addStep(transMeta, new DBCMongoOutputStep(), "MONGODB OUTPUT", "Writing to ServerDB"));
			
			createHop(transMeta);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBCException();
		}
		return transMeta;
	}

	public void execute() throws Exception{
		try{
			DBCEmbedConnectionManager dbcConnectionManager = new DBCEmbedConnectionManager();
			try{
				KettleEnvironment.init();
				TransMeta transMeta = getTransDefinition();
				Trans trans = new Trans(transMeta);
				trans.setLogLevel(LogLevel.DEBUG);
				trans.prepareExecution(null);
				
				Connection connection = dbcConnectionManager.getConnection();
				
				StepInterface step = trans.getStepInterface("FETCH DB", 0);
				step.addRowListener(new DBCFetchAdapter(connection, trans.getLogChannel()));
				
//					step = trans.getStepInterface("MAPPING", 0);
//					step.addRowListener(new DBCMappingAdapter(connection,trans.getLogChannel()));
				
//					step = trans.getStepInterface("FILTER", 0);
//					step.addRowListener(new DBCFilterAdapter(trans.getLogChannel()) );
				
//					step = trans.getStepInterface("MONGODB OUTPUT", 0);
//					step.addRowListener(new DBCMongoOutputAdapter(trans.getLogChannel()) );
//					
				trans.startThreads();
				trans.waitUntilFinished();
				
				Result result = trans.getResult();
				
				// report on the outcome of the transformation
				String outcome = "executed "+ (result.getNrErrors() == 0 ? "successfully": "with " + result.getNrErrors()+ " errors")+"\n";
				trans.getLogChannel().logBasic(outcome);
				
			}finally{
				dbcConnectionManager.closeConnection();
			}
		}catch(Exception e){
			throw e;
		}
	}
}
