package com.graymatter.askme.connector.exception;

@SuppressWarnings("serial")
public class DBCAccessDataException extends Exception {

	public DBCAccessDataException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DBCAccessDataException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DBCAccessDataException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DBCAccessDataException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
