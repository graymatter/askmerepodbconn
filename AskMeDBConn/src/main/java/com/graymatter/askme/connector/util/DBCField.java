package com.graymatter.askme.connector.util;

import com.graymatter.askme.connector.util.DBCConstants.SQLConstants;

public class DBCField {

	public static String getAliasFromColumnName(String columnName) throws Exception{
		if(columnName.contains(SQLConstants.OPERATOR_AS)){
			String[] tokens = columnName.split(SQLConstants.OPERATOR_AS);
			columnName = tokens[1];
		}
		return columnName;
	}
	
	public static String geColumnNameFromAlias(String columnName) throws Exception{
		if(columnName.contains(SQLConstants.OPERATOR_AS)){
			String[] tokens = columnName.split(SQLConstants.OPERATOR_AS);
			columnName = tokens[0];
		}
		return columnName;
	}

}
