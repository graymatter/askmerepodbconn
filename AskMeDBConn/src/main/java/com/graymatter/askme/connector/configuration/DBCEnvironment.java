package com.graymatter.askme.connector.configuration;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.mortbay.util.ajax.WaitingContinuation;
import org.pentaho.di.core.Const;
import org.pentaho.di.core.xml.XMLHandler;
import org.w3c.dom.Node;

import com.graymatter.askme.common.exception.ASKPersistenceThrow;
import com.graymatter.askme.connector.common.bean.AskMeLog;
import com.graymatter.askme.connector.common.bean.AskMeLog.ASKE_ME_LOG_ACTION;
import com.graymatter.askme.connector.common.bean.AskMeLog.ASK_ME_LOG_DESC;
import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.JobConstants;
import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.XMLEnvelopeConstants;
import com.graymatter.askme.connector.common.util.mongoUtil.MongoUtility;
import com.graymatter.askme.connector.exception.DBCErrorCode;
import com.graymatter.askme.connector.exception.DBCException;
import com.graymatter.askme.connector.persistence.DBCMongoConnectionManager;
import com.graymatter.askme.connector.util.DBCConstants;
import com.graymatter.askme.connector.util.DBCConstants.DBFetchConstants;

public class DBCEnvironment {
	private static Map<String, Map<String, String>> env;
	private static List<Map<String, String>> envFields;
	private static String PATH;
	
	public static Map<String, Map<String, String>> getEnv(){
		return env;
	}
	
	public static List<Map<String, String>> getEnvFields(){
		return envFields;
	}
	
	private static void makeJobEnv(Node job) throws Exception{
		Map<String, String> jobEnv = new HashMap<String, String>();
 		jobEnv.put(JobConstants.PATH, PATH);

		for(int j=0; j<JobConstants.getAttribute().length; j++){
			String currentAttr = JobConstants.getAttribute()[j];
			String attrValue = XMLHandler.getTagAttribute(job, currentAttr);
			if(!Const.isEmpty(attrValue)) jobEnv.put(currentAttr, attrValue);
		}
		
		env.put(XMLEnvelopeConstants.JOB_ENV, jobEnv);
	}
	
	private static void makeConf(Node conf) throws Exception{
		Map<String, String> confEnv = new HashMap<String, String>();
		confEnv.put(XMLEnvelopeConstants.DB_CONN_ATTEMPS,  XMLHandler.getTagAttribute(conf, XMLEnvelopeConstants.DB_CONN_ATTEMPS));
		env.put(XMLEnvelopeConstants.STARTER, confEnv);
	}
	
	private static void makeDBToFetchEnv(Node job) throws Exception{
		Map<String, String> dbFetchEnv = new HashMap<String, String>();
		dbFetchEnv.put(XMLEnvelopeConstants.HOSTNAME,  XMLHandler.getTagValue(job, DBFetchConstants.DB_TO_FETCH, XMLEnvelopeConstants.HOSTNAME));
		dbFetchEnv.put(XMLEnvelopeConstants.PORT,  XMLHandler.getTagValue(job, DBFetchConstants.DB_TO_FETCH, XMLEnvelopeConstants.PORT));
		dbFetchEnv.put(XMLEnvelopeConstants.DBTYPE,  XMLHandler.getTagValue(job, DBFetchConstants.DB_TO_FETCH,  XMLEnvelopeConstants.DBTYPE));
		dbFetchEnv.put(XMLEnvelopeConstants.ACCESS,  XMLHandler.getTagValue(job, DBFetchConstants.DB_TO_FETCH,  XMLEnvelopeConstants.ACCESS));
		dbFetchEnv.put(XMLEnvelopeConstants.DBNAME,  XMLHandler.getTagValue(job, DBFetchConstants.DB_TO_FETCH,  XMLEnvelopeConstants.DBNAME));
		dbFetchEnv.put(XMLEnvelopeConstants.USER,  XMLHandler.getTagValue(job, DBFetchConstants.DB_TO_FETCH,  XMLEnvelopeConstants.USER));
		dbFetchEnv.put(XMLEnvelopeConstants.PWD,  XMLHandler.getTagValue(job, DBFetchConstants.DB_TO_FETCH,  XMLEnvelopeConstants.PWD));
		env.put(DBFetchConstants.DB_TO_FETCH, dbFetchEnv);
	}
	
	private static void makeLocalDBEnv(Node job) throws Exception{
		Map<String, String> dbLocalEnv = new HashMap<String, String>();
		dbLocalEnv.put(XMLEnvelopeConstants.HOSTNAME,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.LOCAL_DATABASE_ENV, XMLEnvelopeConstants.HOSTNAME));
		dbLocalEnv.put(XMLEnvelopeConstants.PORT,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.LOCAL_DATABASE_ENV, XMLEnvelopeConstants.PORT));
		dbLocalEnv.put(XMLEnvelopeConstants.DBTYPE,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.LOCAL_DATABASE_ENV,  XMLEnvelopeConstants.DBTYPE));
		dbLocalEnv.put(XMLEnvelopeConstants.ACCESS,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.LOCAL_DATABASE_ENV,  XMLEnvelopeConstants.ACCESS));
		dbLocalEnv.put(XMLEnvelopeConstants.DBNAME,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.LOCAL_DATABASE_ENV,  XMLEnvelopeConstants.DBNAME));
		dbLocalEnv.put(XMLEnvelopeConstants.USER,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.LOCAL_DATABASE_ENV,  XMLEnvelopeConstants.USER));
		dbLocalEnv.put(XMLEnvelopeConstants.PWD,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.LOCAL_DATABASE_ENV,  XMLEnvelopeConstants.PWD));
		env.put(XMLEnvelopeConstants.LOCAL_DATABASE_ENV, dbLocalEnv);
	}
	
	private static void makeRemoteDBEnv(Node job) throws Exception{
		Map<String, String> dbRemoteEnv = new HashMap<String, String>();
		dbRemoteEnv.put(XMLEnvelopeConstants.HOSTNAME,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.REMOTE_DATABASE_ENV, XMLEnvelopeConstants.HOSTNAME));
		dbRemoteEnv.put(XMLEnvelopeConstants.PORT,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.REMOTE_DATABASE_ENV, XMLEnvelopeConstants.PORT));
		dbRemoteEnv.put(XMLEnvelopeConstants.DBTYPE,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.REMOTE_DATABASE_ENV,  XMLEnvelopeConstants.DBTYPE));
		dbRemoteEnv.put(XMLEnvelopeConstants.ACCESS,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.REMOTE_DATABASE_ENV,  XMLEnvelopeConstants.ACCESS));
		dbRemoteEnv.put(XMLEnvelopeConstants.DBNAME,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.REMOTE_DATABASE_ENV,  XMLEnvelopeConstants.DBNAME));
		dbRemoteEnv.put(XMLEnvelopeConstants.USER,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.REMOTE_DATABASE_ENV,  XMLEnvelopeConstants.USER));
		dbRemoteEnv.put(XMLEnvelopeConstants.PWD,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.REMOTE_DATABASE_ENV,  XMLEnvelopeConstants.PWD));
		env.put(XMLEnvelopeConstants.REMOTE_DATABASE_ENV, dbRemoteEnv);
	}
	
	private static void makeSqlEnv(Node job) throws Exception{
		Map<String, String> sqlEnv = new HashMap<String, String>();
		sqlEnv.put(DBCConstants.SQLConstants.SELECT, XMLHandler.getTagValue(job, XMLEnvelopeConstants.SQL_ENV, DBCConstants.SQLConstants.SELECT));
		sqlEnv.put(DBCConstants.SQLConstants.PRIMARY_KEY, XMLHandler.getTagValue(job, XMLEnvelopeConstants.SQL_ENV, DBCConstants.SQLConstants.PRIMARY_KEY));
		
		Node fields = XMLHandler.getSubNode(job, XMLEnvelopeConstants.SQL_ENV, DBCConstants.SQLConstants.FIELDS);
		List<Node> nl = XMLHandler.getNodes(fields, DBCConstants.SQLConstants.FIELD);
		for(int i=0; i<nl.size(); i++){
			if(envFields==null) envFields = new ArrayList<Map<String,String>>();
			Node currentNode = nl.get(i);
			String value =  XMLHandler.getNodeValue(currentNode);
			
			Map<String,String> field = new LinkedHashMap<String, String>();
			field.put(DBCConstants.SQLConstants.FIELD, value);
			for(int j=0; j<DBCConstants.SQLConstants.getAttribute().length; j++){
				String currentAttr = DBCConstants.SQLConstants.getAttribute()[j];
				String attrValue = XMLHandler.getTagAttribute(currentNode, currentAttr);
				if(!Const.isEmpty(attrValue)) field.put(currentAttr, attrValue);
			}
			envFields.add(field);
		}
		
		env.put(XMLEnvelopeConstants.SQL_ENV, sqlEnv);
		if(Const.isEmpty(envFields)) throw new DBCException(DBCErrorCode.DBFetch.FETCH_NOFIELDS);
	}
	
	public static void initJob(String path, Node job) throws Exception{
		if(env==null) env = new HashMap<String, Map<String,String>>();
		PATH = path;
		makeJobEnv(job);
		makeDBToFetchEnv(job);
		makeRemoteDBEnv(job);
		makeSqlEnv(job);
	}
	
	public static void initConf(String path, Node conf) throws Exception{
		if(env==null) env = new HashMap<String, Map<String,String>>();
		PATH = path;
		makeConf(conf);
		makeLocalDBEnv(conf);
	}
	
	public static void release(){
		env = null;
		envFields = null;
	}
	
	public static void initLocalMongoCollection() throws ASKPersistenceThrow, Exception{
		
		DBCMongoConnectionManager localConnectionManager;
		DBCMongoConnectionManager remoteConnectionManager;
		
		try{
			String localHost = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.HOSTNAME);
			Integer localPort = Integer.parseInt(DBCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.PORT));
			String localDbName = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.DBNAME);
			
			String remoteHost = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.REMOTE_DATABASE_ENV).get(XMLEnvelopeConstants.HOSTNAME);
			Integer remotePort = Integer.parseInt(DBCEnvironment.getEnv().get(XMLEnvelopeConstants.REMOTE_DATABASE_ENV).get(XMLEnvelopeConstants.PORT));
			String remoteDbName = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.REMOTE_DATABASE_ENV).get(XMLEnvelopeConstants.DBNAME);
			
			String collectionName = DBFetchConstants.PREFIX +
								    DBCEnvironment.getEnv().get(XMLEnvelopeConstants.JOB_ENV).get(JobConstants.ATTR_NAME).replace(" ", "_") +
								    DBFetchConstants.SUFFIX;
			
			localConnectionManager = new DBCMongoConnectionManager(localDbName, localHost, localPort);
			remoteConnectionManager = new DBCMongoConnectionManager(remoteDbName, remoteHost, remotePort);
			
			MongoUtility mongoUtility = new MongoUtility(localConnectionManager, remoteConnectionManager, collectionName);
			mongoUtility.fromRemoteToLocal();
			
			AskMeLog actionLog = new AskMeLog(DBCConstants.DBC_LOG_TABLE, ASKE_ME_LOG_ACTION.DBC_ACTION, new Date(), ASK_ME_LOG_DESC.INIT_LOCAL_OK, null);
			logAction(actionLog);
		}
		catch(ASKPersistenceThrow e){
			AskMeLog excLog = new AskMeLog(DBCConstants.DBC_LOG_TABLE, ASKE_ME_LOG_ACTION.DBC_ACTION, new Date(), ASK_ME_LOG_DESC.INIT_LOCAL_KO, e.getMessage());
			logAction(excLog);
			throw e;
		}
		catch(Exception e){
			AskMeLog excLog = new AskMeLog(DBCConstants.DBC_LOG_TABLE, ASKE_ME_LOG_ACTION.DBC_ACTION, new Date(), ASK_ME_LOG_DESC.INIT_LOCAL_KO, e.getMessage());
			logAction(excLog);
			throw e;
		}
	}
	
	
	public static void writeRemoteMongoCollection(int dbConnectionAttemps) throws ASKPersistenceThrow, Exception{
		
		DBCMongoConnectionManager localConnectionManager;
		DBCMongoConnectionManager remoteConnectionManager;
		
		try{
			String remoteHost = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.REMOTE_DATABASE_ENV).get(XMLEnvelopeConstants.HOSTNAME);
			Integer remotePort = Integer.parseInt(DBCEnvironment.getEnv().get(XMLEnvelopeConstants.REMOTE_DATABASE_ENV).get(XMLEnvelopeConstants.PORT));
			String remoteDbName = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.REMOTE_DATABASE_ENV).get(XMLEnvelopeConstants.DBNAME);
			
			String localHost = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.HOSTNAME);
			Integer localPort = Integer.parseInt(DBCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.PORT));
			String localDbName = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.DBNAME);
			
			String collectionName = DBFetchConstants.PREFIX +
								    DBCEnvironment.getEnv().get(XMLEnvelopeConstants.JOB_ENV).get(JobConstants.ATTR_NAME).replace(" ", "_") +
								    DBFetchConstants.SUFFIX;
			
			localConnectionManager = new DBCMongoConnectionManager(localDbName, localHost, localPort);
			remoteConnectionManager = new DBCMongoConnectionManager(remoteDbName, remoteHost, remotePort);
			
			MongoUtility mongoUtility = new MongoUtility(localConnectionManager, remoteConnectionManager, collectionName);
			mongoUtility.fromLocalToRemote();
			
			AskMeLog actionLog = new AskMeLog(DBCConstants.DBC_LOG_TABLE, ASKE_ME_LOG_ACTION.DBC_ACTION, new Date(), ASK_ME_LOG_DESC.WRITE_REMOTE_OK, null);
			logAction(actionLog);
		}
		catch(ASKPersistenceThrow e){
			AskMeLog excLog = new AskMeLog(DBCConstants.DBC_LOG_TABLE, ASKE_ME_LOG_ACTION.DBC_ACTION, new Date(), ASK_ME_LOG_DESC.WRITE_REMOTE_KO, e.getMessage());
			logAction(excLog);
			// Se c'è un'eccezione richiama il metodo in modo ricorsivo n volte quante ne vengono configurate
			while(dbConnectionAttemps > 0){
				dbConnectionAttemps = dbConnectionAttemps-1;
				writeRemoteMongoCollection(dbConnectionAttemps);
			}
			throw e;
		}
		catch(Exception e){
			AskMeLog excLog = new AskMeLog(DBCConstants.DBC_LOG_TABLE, ASKE_ME_LOG_ACTION.DBC_ACTION, new Date(), ASK_ME_LOG_DESC.WRITE_REMOTE_KO, e.getMessage());
			logAction(excLog);
			throw e;
		}
	}
	
	private static void logAction(AskMeLog log) {
		DBCMongoConnectionManager localConnectionManager;
		try{
			String localHost = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.HOSTNAME);
			Integer localPort = Integer.parseInt(DBCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.PORT));
			String localDbName = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.DBNAME);
			
			localConnectionManager = new DBCMongoConnectionManager(localDbName, localHost, localPort);
			
			MongoUtility mongoUtility = new MongoUtility(localConnectionManager);
			mongoUtility.logTransformationReceipt(log);
		}catch (ASKPersistenceThrow e) {
			e.printStackTrace();
		}catch (Exception exc){
			exc.printStackTrace();
		}
	}

}
