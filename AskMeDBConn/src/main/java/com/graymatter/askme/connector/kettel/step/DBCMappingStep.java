package com.graymatter.askme.connector.kettel.step;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.pentaho.di.core.Const;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.di.trans.steps.selectvalues.SelectValuesMeta;

import com.graymatter.askme.connector.configuration.DBCEnvironment;
import com.graymatter.askme.connector.exception.DBCErrorCode;
import com.graymatter.askme.connector.exception.DBCException;
import com.graymatter.askme.connector.kettel.general.DBCGeneralStep;
import com.graymatter.askme.connector.util.DBCConstants;
import com.graymatter.askme.connector.util.DBCField;

public class DBCMappingStep extends DBCGeneralStep {
	private String[] sourceFields;
	private String[] targetFields;
	
	private void populateFieldsArray() throws Exception{
		List<String> source = new ArrayList<String>();
		List<String> target = new ArrayList<String>();
		
		List<Map<String, String>> fields = DBCEnvironment.getEnvFields();
		for(Map<String, String> currentField: fields){
			String columnName = DBCField.getAliasFromColumnName(currentField.get(DBCConstants.SQLConstants.FIELD));
			String tagName = Const.isEmpty(currentField.get(DBCConstants.SQLConstants.ATTR_TAG))? columnName: currentField.get(DBCConstants.SQLConstants.ATTR_TAG);
			columnName = getConcatColumn(columnName, currentField);
			source.add(columnName);
			target.add(tagName);
		}
		
		sourceFields = source.toArray(new String[source.size()]);
		targetFields = target.toArray(new String[target.size()]);
	}

	public StepMetaInterface getStep() throws Exception {
		SelectValuesMeta selectValuesMeta = null;
		try {
			populateFieldsArray();
			selectValuesMeta = new SelectValuesMeta();
			selectValuesMeta.allocate(sourceFields.length, 0, 0);
			selectValuesMeta.setSelectName(sourceFields);
			selectValuesMeta.setSelectRename(targetFields);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBCException(DBCErrorCode.Mapping.MAPPING_GENERIC);
		} 
		return selectValuesMeta;
	}

}
