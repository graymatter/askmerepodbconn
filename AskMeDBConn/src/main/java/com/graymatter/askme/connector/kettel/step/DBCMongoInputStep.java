package com.graymatter.askme.connector.kettel.step;

import org.bson.Document;
import org.pentaho.di.core.exception.KettleStepException;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.di.trans.steps.dummytrans.DummyTransMeta;

import com.graymatter.askme.common.exception.ASKPersistenceThrow;
import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.JobConstants;
import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.XMLEnvelopeConstants;
import com.graymatter.askme.connector.configuration.DBCEnvironment;
import com.graymatter.askme.connector.exception.DBCErrorCode;
import com.graymatter.askme.connector.exception.DBCException;
import com.graymatter.askme.connector.kettel.general.DBCGeneralStep;
import com.graymatter.askme.connector.persistence.DBCMongoConnectionManager;
import com.graymatter.askme.connector.util.DBCConstants.DBFetchConstants;
import com.mongodb.client.MongoCollection;

public class DBCMongoInputStep extends DBCGeneralStep {
	
	public DBCMongoInputStep() {
		super();
	}

	public StepMetaInterface getStep() throws Exception {
		
		DBCMongoConnectionManager remoteConnectionManager = null;
		DBCMongoConnectionManager localConnectionManager = null;
		
		try{
			String remoteHost = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.REMOTE_DATABASE_ENV).get(XMLEnvelopeConstants.HOSTNAME);
			Integer remotePort = Integer.parseInt(DBCEnvironment.getEnv().get(XMLEnvelopeConstants.REMOTE_DATABASE_ENV).get(XMLEnvelopeConstants.PORT));
			String remoteDbName = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.REMOTE_DATABASE_ENV).get(XMLEnvelopeConstants.DBNAME);
			
			String localHost = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.HOSTNAME);
			Integer localPort = Integer.parseInt(DBCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.PORT));
			String localDbName = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.DBNAME);
			
			String collectionName = DBFetchConstants.PREFIX +
								    DBCEnvironment.getEnv().get(XMLEnvelopeConstants.JOB_ENV).get(JobConstants.ATTR_NAME).replace(" ", "_") +
								    DBFetchConstants.SUFFIX;
			
			
			remoteConnectionManager = new DBCMongoConnectionManager(remoteDbName, remoteHost, remotePort);
			MongoCollection<Document> remoteContentTableCollection = remoteConnectionManager.getCollection(collectionName);
			
			
//			collectionName = "DBC_Fisso_Interazioni_Consumer_TABLE";
//			localConnectionManager = new DBCMongoConnectionManager(localDbName, localHost, localPort);
//			MongoCollection<Document> localContentTableCollection = localConnectionManager.getCollection(collectionName);
//			remoteContentTableCollection.drop();
//			localContentTableCollection.drop();
			
			
			if(remoteContentTableCollection.count() > 0){
				localConnectionManager = new DBCMongoConnectionManager(localDbName, localHost, localPort);
				localConnectionManager.createCollection(remoteContentTableCollection, collectionName);
			}
		} 
		catch (ASKPersistenceThrow e) {
			throw new KettleStepException(e);
		}
		catch (Exception exc){
			exc.printStackTrace();
			throw new DBCException(DBCErrorCode.DBMongoDB.MONGODB_GENERIC);
		}
		finally{
			try {
				if(remoteConnectionManager != null) remoteConnectionManager.closeClient();
				if(localConnectionManager != null) localConnectionManager.closeClient();
			} catch (ASKPersistenceThrow e) {
				e.printStackTrace();
			}
		}
		
		return new DummyTransMeta();
	}

}
