package com.graymatter.askme.connector.kettel.step;

import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.di.trans.steps.dummytrans.DummyTransMeta;

import com.graymatter.askme.connector.kettel.general.DBCGeneralStep;

public class DBCMongoOutputStep extends DBCGeneralStep {
	
	public DBCMongoOutputStep() {
		super();
	}

	public StepMetaInterface getStep() throws Exception {		
		return new DummyTransMeta();
	}

}
