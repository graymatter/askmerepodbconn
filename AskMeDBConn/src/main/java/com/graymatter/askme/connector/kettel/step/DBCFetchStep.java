package com.graymatter.askme.connector.kettel.step;

import java.util.List;
import java.util.Map;

import org.pentaho.di.core.Const;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.di.trans.steps.tableinput.TableInputMeta;

import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.XMLEnvelopeConstants;
import com.graymatter.askme.connector.configuration.DBCEnvironment;
import com.graymatter.askme.connector.exception.DBCErrorCode;
import com.graymatter.askme.connector.exception.DBCException;
import com.graymatter.askme.connector.kettel.general.DBCGeneralStep;
import com.graymatter.askme.connector.util.DBCConstants;

public class DBCFetchStep extends DBCGeneralStep{
	private DatabaseMeta dbInfo;
	private String rowLimit;

	public DBCFetchStep(DatabaseMeta dbInfo) {
		super();
		this.dbInfo = dbInfo;
	}

	public void setRowLimit(String rowLimit){
		this.rowLimit = rowLimit;
	}
	
	private String getSelect() throws Exception{
		String selectSQL = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.SQL_ENV).get(DBCConstants.SQLConstants.SELECT);
		String primaryKey = DBCEnvironment.getEnv().get(XMLEnvelopeConstants.SQL_ENV).get(DBCConstants.SQLConstants.PRIMARY_KEY);
		
		StringBuffer retrieve = new StringBuffer();
		retrieve.append(primaryKey);

		List<Map<String, String>> fields = DBCEnvironment.getEnvFields();
		for(Map<String, String> currentField: fields){
			retrieve.append(", " + Const.CR);
			String columnName = currentField.get(DBCConstants.SQLConstants.FIELD);
			columnName = getConcatColumn(columnName, currentField);
			retrieve.append(columnName);
		}
		selectSQL = selectSQL.replace("*", retrieve.toString());
		
		return selectSQL;
	}
	
	public StepMetaInterface getStep() throws Exception {
		TableInputMeta tableInputMeta=null;
		try{
			tableInputMeta = new TableInputMeta();
			tableInputMeta.setDatabaseMeta(dbInfo);
			tableInputMeta.setSQL(getSelect());
			tableInputMeta.setLazyConversionActive(true);
			if(rowLimit!=null) tableInputMeta.setRowLimit(rowLimit);
		}catch(Exception e){
			e.printStackTrace();
			throw new DBCException(DBCErrorCode.DBFetch.FETCH_GENERIC);
		}
		return tableInputMeta;
	}
	
}
