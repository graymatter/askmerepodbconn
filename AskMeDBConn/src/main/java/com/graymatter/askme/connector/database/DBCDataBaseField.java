package com.graymatter.askme.connector.database;

public class DBCDataBaseField {
	private String dbFieldName;
	private String xmlFieldName;
	private String type;
	private String format;

	public String getDbFieldName() {return dbFieldName;}
	public void setDbFieldName(String dbFieldName) {this.dbFieldName = dbFieldName;}

	public String getXmlFieldName() {return xmlFieldName;}
	public void setXmlFieldName(String xmlFieldName) {this.xmlFieldName = xmlFieldName;}

	public String getType() {return type;}
	public void setType(String type) {this.type = type;}
	
	public String getFormat() {return format;}
	public void setFormat(String format) {this.format = format;}
}
