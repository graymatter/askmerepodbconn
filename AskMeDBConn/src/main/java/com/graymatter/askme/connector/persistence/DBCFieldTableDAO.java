package com.graymatter.askme.connector.persistence;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.pentaho.di.core.logging.LogChannelInterface;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.core.row.ValueMetaInterface;

import com.graymatter.askme.connector.exception.DBCAccessDataException;
import com.graymatter.askme.connector.persistence.general.DBCPersistenceOperator;

public class DBCFieldTableDAO extends DBCPersistenceOperator {
	private static String TABLE = "DBC_FIELD";

	
	public DBCFieldTableDAO(Connection connection,
			RowMetaInterface rowMeta, LogChannelInterface logger)
			throws DBCAccessDataException {
		super(TABLE, connection, rowMeta, logger);
		makeFields();
	}

	private void makeFields() throws DBCAccessDataException{
		String tableFields = "";
		for(int i=0; i<getRowMeta().getValueMetaList().size();i++){
			ValueMetaInterface valueMetaInterface = getRowMeta().getValueMeta(i);
			String colName = valueMetaInterface.getName();
			if(getColumnNames() == null) setColumnNames(new ArrayList<String>());
			getColumnNames().add(colName);
			tableFields = tableFields + colName + " " + valueMetaInterface.getOriginalColumnTypeName() + ",";
		}
		tableFields = (tableFields + "|").replace(",|", "");
		setTableFields(tableFields);
	}
	
	public void insertFieldName(List<String> fields) throws Exception{
		for(String field: fields){
			String[] row = {field}; 
			insert(row);
		}
	}
	
	public void replaceFieldName(List<String> fields) throws Exception{
		truncate();
		insertFieldName(fields);
	}

}
