package com.graymatter.askme.connector.kettel.general;

import com.graymatter.askme.connector.util.DBCConstants.SQLConstants;

import java.util.Map;

import org.pentaho.di.core.Const;
import org.pentaho.di.trans.step.BaseStepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;

public abstract class DBCGeneralStep  extends BaseStepMeta{
	
	protected String getConcatColumn(String columnName, Map<String, String> currentField){
		if(columnName.contains(SQLConstants.FIELD_SEPARATOR)){
			String separator = Const.isEmpty(currentField.get(SQLConstants.ATTR_SEPARATOR))?SQLConstants.FIELD_SEPARATOR:currentField.get(SQLConstants.ATTR_SEPARATOR);
			columnName = "CONCAT(" + columnName.replace(SQLConstants.FIELD_SEPARATOR, ",'" + separator + "',") + ")";
		}
		return columnName;
	}

	public abstract StepMetaInterface getStep() throws Exception;

}
