package com.graymatter.askme.connector.bean;

import java.io.Serializable;

import org.bson.Document;
import org.bson.types.ObjectId;

import com.graymatter.askme.common.utility.ASKUtility;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

public class DBCDocument implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private ObjectId id;
	private String reference;
	private String title;
//	private int idSection;
//	private String sectionContent;
	private String content;
	private String fingerPrint;
	private String flag;
	
	
	public DBCDocument() {
		super();
	}

	public DBCDocument(ObjectId id, String reference, String title, String flag/*,
					   int idSection, String sectionContent*/) {
		super();
		this.id = id;
		this.reference = reference;
		this.title = title;
//		this.idSection = idSection;
//		this.sectionContent = sectionContent;
		this.flag = flag;
	}

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

//	public int getIdSection() {
//		return idSection;
//	}
//
//	public void setIdSection(int idSection) {
//		this.idSection = idSection;
//	}
//
//	public String getSectionContent() {
//		return sectionContent;
//	}
//
//	public void setSectionContent(String sectionContent) {
//		this.sectionContent = sectionContent;
//	}
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public String getFingerPrint() {
		return fingerPrint;
	}

	public void setFingerPrint(String fingerPrint) {
		this.fingerPrint = fingerPrint;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public static Document createDBObject(DBCDocument dbcDocument) {
		DBObject dbObj = (DBObject) JSON.parse(ASKUtility.ASKJson.GSON.toJson(dbcDocument));
		Document document = new Document(dbObj.toMap());
		return document;
    }

}
