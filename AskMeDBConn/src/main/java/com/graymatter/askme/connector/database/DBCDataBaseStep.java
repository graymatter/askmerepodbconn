package com.graymatter.askme.connector.database;

import com.graymatter.askme.connector.configuration.DBCDataBaseConfig;
import com.graymatter.askme.connector.exception.DBCErrorCode;
import com.graymatter.askme.connector.exception.DBCException;

import org.pentaho.di.core.database.DatabaseMeta;

public class DBCDataBaseStep {

	public DatabaseMeta getDataBaseObject() throws Exception{
		DatabaseMeta databaseMeta = null;
		try{
			DBCDataBaseConfig dbcDataBaseDefinition = new DBCDataBaseConfig();
			String stringConnection = dbcDataBaseDefinition.getStringConnection();

			databaseMeta = new DatabaseMeta(stringConnection);
		}catch(Exception e){
			e.printStackTrace();
			throw new DBCException(DBCErrorCode.DBError.DB_GENERIC);
		}
		return databaseMeta;
	}
}
