package com.graymatter.askme.connector.persistence;

import java.io.Serializable;

import com.graymatter.askme.common.exception.ASKPersistenceThrow;
import com.graymatter.askme.common.mongo.ASKMongoGeneral;


public class DBCMongoConnectionManager extends ASKMongoGeneral implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public DBCMongoConnectionManager(String dbName, String hostName, int hostPort) throws ASKPersistenceThrow, Exception {
		super(dbName, hostName, hostPort);
		try{
			// Trick to verify connection....
			getMongoClient().getAddress();
		} catch (Throwable exc){
			throw new ASKPersistenceThrow();
		}
	}

}
